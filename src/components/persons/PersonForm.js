import React, { useState } from 'react'

export default function PersonForm({onSubmit}) {
    const [person, setPerson] = useState({
        firstName: '',
        lastName: '',
        age: 0
    });
    const handleChange = event => {
        setPerson({
            ...person,
            [event.target.name]: event.target.value
        })
    }
    const handleSubmit = event => {
        event.preventDefault();
        onSubmit(person);
    }
    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="firstName">First Name</label>
            <input onChange={handleChange} type="text" name="firstName" id="firstName" />
            
            <label htmlFor="lastName">Last Name</label>
            <input onChange={handleChange} type="text" name="lastName" id="lastName" />
            
            <label htmlFor="age">Age</label>
            <input onChange={handleChange} type="number" name="age" id="age" />

            <button>Submit</button>
        </form>
    )
}
