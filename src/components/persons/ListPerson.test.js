import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import ListPerson from './ListPerson';

const list = [
    {id: 1, firstName: 'First1', lastName: 'Last1', age: 1},
    {id: 2, firstName: 'First2', lastName: 'Last2', age: 2},
    {id: 3, firstName: 'First3', lastName: 'Last3', age: 3}
];

test('component should display 3 elements', () => {
    
    const {getAllByText } = render(<ListPerson persons={list} />);
    const elements = getAllByText(/First[1-3] Last[1-3]/);

    expect(elements.length).toBe(3);
});


test('component should emit person on click', () => {
    
    const clickEvent = jest.fn();
    
    const {getByText} = render(<ListPerson persons={list} onClickPerson={clickEvent} />);
    const element = getByText(/First1/);

    fireEvent.click(element);

    expect(clickEvent).toHaveBeenCalled();
    expect(clickEvent).toHaveBeenCalledWith({id: 1, firstName: 'First1', lastName: 'Last1', age: 1});
});