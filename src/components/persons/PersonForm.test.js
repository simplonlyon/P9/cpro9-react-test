import React from 'react'
import { render, fireEvent } from "@testing-library/react"
import PersonForm from "./PersonForm"



test('should call prop on submit', () => {
    const submitEvent = jest.fn();

    const {getByText} = render(<PersonForm onSubmit={submitEvent} />);

    const submit = getByText('Submit');
    fireEvent.click(submit);

    expect(submitEvent).toHaveBeenCalled();

});

test('should emit a person on submit', () => {
    const submitEvent = jest.fn();

    const {getByText, getByLabelText} = render(<PersonForm onSubmit={submitEvent} />);

    const submit = getByText('Submit');
    
    const inputFirst = getByLabelText('First Name');
    const inputLast = getByLabelText('Last Name');
    const inputAge = getByLabelText('Age');

    fireEvent.change(inputFirst, { target: { value: 'Test' } });
    fireEvent.change(inputLast, { target: { value: 'TestLast' } });
    fireEvent.change(inputAge, { target: { value: 43 } });

    fireEvent.click(submit);
    expect(submitEvent).toHaveBeenCalled();
    expect(submitEvent).toHaveBeenCalledWith({firstName: 'Test', lastName: 'TestLast', age: '43'});

});