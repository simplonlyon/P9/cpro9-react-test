import React from 'react'
import { render, fireEvent, wait } from "@testing-library/react"
import PersonManager from './PersonManager';

import {fetchPersons, postPerson} from '../../services/person-service';

jest.mock('../../services/person-service');

beforeEach(() => {

    const list = [
        {id: 1, firstName: 'First1', lastName: 'Last1', age: 1},
        {id: 2, firstName: 'First2', lastName: 'Last2', age: 2},
        {id: 3, firstName: 'First3', lastName: 'Last3', age: 3}
    ];
    fetchPersons.mockResolvedValue(list);
    postPerson.mockResolvedValue({id:4, firstName:'Test', lastName:'TestLast', age: 34});
});

afterEach(() => fetchPersons.mockClear());

test('component should display 3 elements', async () => {
    
    const { getAllByText } = render(<PersonManager />);
    
    expect(fetchPersons).toHaveBeenCalled();

    await wait(() => {
        const elements = getAllByText(/First[1-3] Last[1-3]/);
        expect(elements.length).toBe(3);
    });
    

});


test('should display a new person on form submit', async () => {
    
    const { getByText } = render(<PersonManager />);
    const submit = getByText('Submit');
    fireEvent.click(submit);

    await wait(() => {
        getByText('Test TestLast');
    });
    

});