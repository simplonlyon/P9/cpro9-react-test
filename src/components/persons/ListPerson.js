import React from 'react'

export default function ListPerson({persons, onClickPerson}) {
    return (
        <ul>
            {persons.map(person => (
                <li onClick={() => onClickPerson(person)} key={person.id}>
                    <p>{person.firstName} {person.lastName}</p>
                    </li>
            ))}
        </ul>
    )
}