import React, { useEffect, useState } from 'react'
import ListPerson from './ListPerson'
import { fetchPersons, postPerson } from '../../services/person-service';
import PersonForm from './PersonForm';

export default function PersonManager() {
    const [persons, setPersons] = useState([]);

    useEffect(() => {
        fetchPersons().then(data => setPersons(data));
    }, []);

    const addPerson = (person) => {
        postPerson(person).then(data => setPersons([...persons, data]));
    }

    return (
        <div>
            <ListPerson persons={persons} />
            <PersonForm onSubmit={addPerson} />
        </div>
    )
}
