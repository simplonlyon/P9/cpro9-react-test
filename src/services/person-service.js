

export  async function fetchPersons() {
    const response = await fetch('http://localhost:3000/api/persons');
    return response.json();
}


export  async function postPerson(person) {
    const response = await fetch('http://localhost:3000/api/persons', {
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.parse(person),
        method: 'POST'
    });
    return response.json();
}

export  async function patchPerson(person) {
    const response = await fetch('http://localhost:3000/api/persons/'+person.id, {
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.parse(person),
        method: 'PATCH'
    });
    return response.json();
}

export  async function deletePerson(person) {
    const response = await fetch('http://localhost:3000/api/persons/'+person.id, {
        method: 'DELETE'
    });
    return response.json();
}

export  async function fetchPerson(id) {
    const response = await fetch('http://localhost:3000/api/persons/'+id);
    return response.json();
}

