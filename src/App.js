import React from 'react';

import './App.css';
import PersonManager from './components/persons/PersonManager';

function App() {
  return (
    <div className="App">
      <PersonManager />
    </div>
  );
}

export default App;
